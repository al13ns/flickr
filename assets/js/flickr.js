/**
 *  simple flickr api js connector.
 *
 *  some methods have an optional "callback" argument. if this argument is specified,
 *  then the jqXHR request is performed _asynchronously_ and callback gets called on success and null is returned.
 *  if it is not specified, then the jqXHR request is performed _synchronously_ and it's response is returned.
 *
 * @param {string}    api_key
 * @constructor
 */
var Flickr	=	function( args )
{
    var apiKey  =   args.api_key;

    if( !apiKey )
    {
        throw "Mandatory arguments must be set - api_key";
    }

    /**
     *
     * @param {string}      userId
     * @param {function}    callback
     * @returns {object|null}
     */
    this.getUserInfo   =   function( userId , callback )
    {
        return this.callMethod( 'people.getInfo' , callback , { 'user_id'   :   userId  });
    };

	/**
     *
	 * calls flickr api method. 
	 * if callback is specified, returns null and runs asynchronous request.
	 * if callback is not specified, runs synchronous request and returns it's response.
	 *
     * @param {string}		method
     * @param {function}   callback	 
     * @param {object}     data
     * @returns {object|null}
     */
    this.callMethod    =   function( method , callback , data )
    {
        var _data =   null;
        var self  =   this;

        data = data ? data : {};
        data.api_key    =   apiKey;
        data.extras     =   'original_format';

        $.ajax(
            {
                'async' :   callback ? true : false ,
                'url'   :   'https://www.flickr.com/services/rest/?method=flickr.'+ method +'&format=json&nojsoncallback=1' ,
                'data'  :   data,
                'success':   function( data )
                {
                    if( 'fail' ==  data.stat )
                    {
                        console.log( data.message );
                        throw data.message;
                    }

                    if( callback )
                    {
                        callback.call( self , data );
                    }
                    else
                    {
                        _data =   data;
                    }
                }
            });

        return _data;
    };

    /**
     *
     * @param {string}      photoId
     * @param {function}    callback
     * @returns {object|null}
     */
    this.getPhotoInfo   =   function( photoId , callback )
    {
        return this.callMethod( 'photos.getInfo' , callback , { 'photo_id' : photoId } );
    };

    /**
     *
     * @param {string}      userId
     * @param {integer}     page
     * @param {integer}     perPage
     * @param {function}    callback
     * @returns {object|null}
     */
    this.getPhotos  =   function( userId , page , perPage , callback )
    {
        return this.callMethod( 'people.getPhotos' , callback ,
            {
                'user_id'   :   userId ,
                'per_page'	:	perPage ,
                'page'		:	page
            });
    };


    /**
     *
         s	small square 75x75
         q	large square 150x150
         t	thumbnail, 100 on longest side
         m	small, 240 on longest side
         n	small, 320 on longest side
         z	medium 640, 640 on longest side
         c	medium 800, 800 on longest side†
         b	large, 1024 on longest side
         h
         k
        sq

         flickr size suffixes - https://www.flickr.com/services/api/misc.urls.html
     *
     * @param {object|string}   photo
     * @param {string}          size
     * @returns {string}
     */
    this.getPhotoUrl    =   function( photo , size )
    {
        if( size && -1 == [ 's' , 'q' , 't' , 'm' , 'n' , 'z' , 'c' , 'b' , 'h' , 'k' , 'sq' ].indexOf( size ) )
        {
            throw "Invalid size.";
        }

        if( 'object' != typeof photo )
        {
            photo   =   this.getPhotoInfo( photo );
        }

        if( size )
        {
            return 'https://farm'+ photo.farm +'.staticflickr.com/'+ photo.server +'/'+ photo.id +'_'+ photo.secret +'_'+ size +'.jpg';
        }
        else
        {
            return 'https://farm'+ photo.farm +'.staticflickr.com/'+ photo.server +'/'+ photo.id +'_'+ photo.secret +'.jpg';
        }
    };


    /**
     * callback receives the url as an argument
     *
     * @param {object|string}   photo
     * @param {function}    callback
     * @returns {string}
     */
    this.getPhotoOriginalUrl    =   function( photo , callback )
    {
        if( 'object' != typeof photo )
        {
            photo   =   this.getPhotoInfo( photo );
        }

        if( photo['originalsecret '] )
        {
            var url    =  'https://farm'+ photo.farm +'.staticflickr.com/'+ photo.server +'/'+ photo.id +'_'+ photo.originalsecret +'_o.'+ photo.originalformat;

            if( callback )
            {
                var self  =   this;
                callback.call( self , url );
            }
            else
            {
                return url;
            }
        }
        else
        {
            if( callback )
            {
                var self  =   this;
                this.getPhotoSizes( photo.id ,
                function( data )
                {
                    callback.call( self , data.sizes.size.pop().source );
                });
            }
            else
            {
                return this.getPhotoSizes( photo.id ).sizes.size.pop().source;
            }
        }
    };

    /**
     *
     * @param {string}      photoId
     * @param {function}    callback
     * @returns {object|null}
     */
    this.getPhotoSizes  =   function( photoId , callback )
    {
        return this.callMethod( 'photos.getSizes' , callback , { 'photo_id'   :   photoId  });
    };
};
