/**
 *
 * @param {Flickr}
 *
 * @param {string}    user_id
 * @param {int}       per_page
 * @param {string}    size
 * @param {element}   photos_element
 * @param {element}   pager_element
 * @param {bool}      justifiedgallery
 * @param {bool}      fancybox
 * @constructor
 */
var Gallery =   function( Flickr , args )
{
    this.Flickr =   Flickr;

    var userId      =   args.user_id;
    var photosEle   =   args.photos_element;
    var pagerEle    =   args.pager_element;
    var perPage     =   args.per_page ? args.per_page : 25;
    var size        =   args['size'] ? args['size'] : null;
    var jg          =   undefined === args.justifiedgallery ? ( true ) : ( args.justifiedgallery ? true : false );
    var fb          =   undefined === args.fancybox ? ( true ) : ( args.fancybox ? true : false );

    if( !userId || !photosEle || !pagerEle )
    {
        throw "Mandatory arguments must be set - user_id , photos_element, pager_element";
    }

    var	fillPhotos	=	function( photoObj )
    {
        $( photosEle ).empty();

        for( var i in photoObj.photo )
        {
            var photo   =   photoObj.photo[i];
            var src     =   this.Flickr.getPhotoUrl( photo , size );

            var img =   $( '<img />' )
                        .attr( 'src' , src )
                        .attr( 'alt' , photo.title )
                        .attr( 'title' , photo.title );

            var a   =   $( '<a />' )
                        .attr( 'target' , '_blank' )
                        .attr( 'href' , src );

            $( a ).append( img );
            $( photosEle ).append( a );

            this.Flickr.getPhotoOriginalUrl( photo ,
            function( a )
            {
                return function( url )
                {
                    $( a ).attr( 'href' , url );
                };
            }(a)
            );
        }

        if( jg )
        {
            $( photosEle ).justifiedGallery();
        }

        if( fb )
        {
            $( photosEle ).find( 'a' ).fancybox({
                helpers: {
                    title: {
                        type: 'over'
                    }
                }
            });
        }
    };

    var	fillPager	=	function( data )
    {
        $( pagerEle ).empty();
        var self    =   this;

        var _callback   =   function( i )
        {
            return function(){ self.loadPage( i ); };
        };

        for( var i = 1 ; i <= data.pages ; i++ )
        {
            if( i == data.page )
            {
                $( pagerEle ).append( $( '<a href="#" />' )
                                                .text( ' '+ i +' ' )
                                                .click( _callback( i ) )
                                                .addClass( 'active' ) );
            }
            else
            {
                $( pagerEle ).append( $( '<a href="#" />' ).text( ' '+ i +' ' ).click( _callback( i ) ) );
            }

        }
    }

    /**
     *
     * @param {integer} page
     */
    this.loadPage   =   function( page )
    {
        var self    =   this;

        this.Flickr.getPhotos( userId , page , perPage ,
        function( data )
        {
            fillPhotos.call( self , data.photos );
            fillPager.call( self , data.photos );
        });
    };

};